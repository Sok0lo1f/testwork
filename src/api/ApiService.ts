import axios from "axios"
import { IPost } from "../interfaces/Post"

export class ApiService {
    private URL = ' https://jsonplaceholder.typicode.com'

    getPosts = () => axios.get<IPost[]>(`${this.URL}/posts`)

}