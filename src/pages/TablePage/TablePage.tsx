import React, { useEffect, useMemo } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Input } from '../../components/Input/Input'
import { Loader } from '../../components/Loader/Loader'
import { Pagination } from '../../components/Pagination/Pagination'
import { Table } from '../../components/Table/Table'
import { useActions } from '../../hooks/useActions'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { ITableHeader } from '../../interfaces/Table'

import classes from './TablePage.module.css'

const TableHeaders: ITableHeader[] = [
    {
        name: 'ID',
        property: 'id',
    },
    {
        name: 'Заголовок',
        property: 'title',
    },
    {
        name: 'Описание',
        property: 'body',
    },
]
export const TablePage = () => {
    const { page } = useParams()
    const { getPosts } = useActions()
    const { posts, loading, error, pages } = useTypedSelector((state) => state.postReducer)
    const nav = useNavigate()

    useEffect(() => {
        getPosts()
    }, [])

    useEffect(() => {
        nav('/1')
    }, [pages])

    const visiblePosts = useMemo(() => {
        if (posts.length === 0) return []
        const start = 10 * (Number(page) - 1 || 0)
        return posts.slice(start, start + 10)
    }, [posts, page])

    if (loading || error) return <Loader />

    return (
        <div className={classes.Page}>
            <div className={classes.PageInner}>
                <Input />
                <Table headers={TableHeaders} posts={visiblePosts} />
                <Pagination activePage={Number(page) || 1} />
            </div>
        </div>
    )
}
