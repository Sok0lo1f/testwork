import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IPost } from '../../interfaces/Post'
import { getPosts } from '../actions/getPosts'

interface PostState {
    posts: IPost[]
    error: string
    loading: boolean
    apiPost: IPost[]
    pages: number
}

const initialState: PostState = {
    posts: [],
    apiPost: [],
    error: '',
    loading: false,
    pages: 1
}

export const postSlice = createSlice({
    name: 'posts',
    initialState,
    reducers: {
        searchPost: (state: PostState, action: PayloadAction<string>) => {
            if(action.payload.trim().length === 0) {
                state.posts = [...state.apiPost]
                state.pages = Math.ceil(state.apiPost.length / 10)
            } 
            else {
                const newPosts = state.apiPost.filter(post => {
                    if(post.body.toLowerCase().includes(action.payload.toLowerCase()) 
                        || post.title.toLowerCase().includes(action.payload.toLowerCase())
                        || post.id === Number(action.payload)) 
                        return true
                    return false
                })
                state.posts = [...newPosts]
                state.pages = Math.ceil(newPosts.length / 10)
            }
            
        },
        sortBy: (state: PostState, action: PayloadAction<[string, 'desc' | 'asc']>) => {
            const property = action.payload[0]
            const type = action.payload[1]
            state.posts.sort((a, b) => {
                if(type === 'asc') {
                    if(typeof a[property] === 'number') {
                        return Number(a[property]) - Number(b[property])
                    } else return +(a[property] > b[property])  || -(a[property] < b[property])
                    
                } else {
                    if(typeof a[property] === 'number') {
                        return Number(b[property]) - Number(a[property])
                    } else return +(b[property] > a[property])  || -(b[property] < a[property])
                    
                }
            })
        },
    },
    extraReducers: {
        [getPosts.pending.type]: (state: PostState) => {
            state.loading = true
            state.error = ''
            state.posts = []
            state.apiPost = []
            state.pages = 1
        },
        [getPosts.fulfilled.type]: (state: PostState, action: PayloadAction<IPost[]>) => {
            state.loading = false
            state.apiPost = action.payload
            state.posts = action.payload
            state.pages = Math.ceil(action.payload.length / 10)
        },
        [getPosts.rejected.type]: (state: PostState, action: PayloadAction<never, never, never, Error>) => {
            state.loading = false
            state.error = action.error.message
            alert(action.error.message)
        }
    }
})

export const postActions = postSlice.actions
export const postReducer = postSlice.reducer