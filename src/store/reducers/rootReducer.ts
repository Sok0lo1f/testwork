import { combineReducers } from '@reduxjs/toolkit'
import { postReducer } from './postsReducer'

export const rootReducer = combineReducers({
    postReducer
})
export type RootState = ReturnType<typeof rootReducer>