import { createAsyncThunk } from '@reduxjs/toolkit'
import { ApiService } from '../../api/ApiService'

export const getPosts = createAsyncThunk('posts/get', async() => {
    const api = new ApiService()
    const response = await api.getPosts()
    return response.data
})