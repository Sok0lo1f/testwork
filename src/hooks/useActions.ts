import { bindActionCreators } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import { getPosts } from '../store/actions/getPosts'
import { AppDispatch } from '../store/store'

export const useActions = () => {
    const dispatch = useDispatch<AppDispatch>()
    return bindActionCreators({
        getPosts
    }, dispatch)
}