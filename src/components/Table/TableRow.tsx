import React from 'react'
import { IPost } from '../../interfaces/Post'
import { ITableHeader } from '../../interfaces/Table'
import classes from './Table.module.css'

interface TableRowProps {
    post: IPost
    headers: ITableHeader[]
}
export const TableRow = ({ post, headers }: TableRowProps) => {
    return (
        <div className={classes.TableRow}>
            {headers.map((h) => (
                <div key={h.property}>{post[h.property] || '-'}</div>
            ))}
        </div>
    )
}
