import clsx from 'clsx'
import React, { useEffect, useState } from 'react'
import { IPost } from '../../interfaces/Post'
import { ITableHeader } from '../../interfaces/Table'
import classes from './Table.module.css'
import { TableRow } from './TableRow'
import ArrowIco from '../../assets/arrow.svg'
import { useDispatch } from 'react-redux'
import { AppDispatch } from '../../store/store'
import { postActions } from '../../store/reducers/postsReducer'
import { useTypedSelector } from '../../hooks/useTypedSelector'

interface TableProps {
    headers: ITableHeader[]
    posts: IPost[]
}
interface SortType {
    property: string
    type: 'asc' | 'desc'
}
const defaultSort: SortType = {
    property: 'id',
    type: 'asc',
}
export const Table = ({ headers, posts }: TableProps) => {
    const [sortBy, setSortBy] = useState<SortType>(defaultSort)
    const dispatch = useDispatch<AppDispatch>()
    const { pages } = useTypedSelector((state) => state.postReducer)
    const onHeaderClick = (property: string) => {
        setSortBy((prev) => ({
            property,
            type: prev.property === property ? (prev.type === 'asc' ? 'desc' : 'asc') : prev.type,
        }))
    }
    useEffect(() => {
        setSortBy(defaultSort)
    }, [pages])
    useEffect(() => {
        dispatch(postActions.sortBy([sortBy.property, sortBy.type]))
    }, [sortBy])
    return (
        <div className={classes.Table}>
            <div className={clsx(classes.TableRow, classes.TableHeader)}>
                {headers.map((h) => (
                    <div key={h.property} onClick={() => onHeaderClick(h.property)}>
                        {h.name}{' '}
                        {sortBy.property === h.property && (
                            <img
                                className={clsx(
                                    classes.Arrow,
                                    sortBy.type === 'asc' && classes.ArrowRotate
                                )}
                                src={ArrowIco}
                            />
                        )}
                    </div>
                ))}
            </div>
            {posts.length > 0 ? (
                posts.map((post) => <TableRow key={post.id} post={post} headers={headers} />)
            ) : (
                <p className={classes.NotFound}>Совпадений не найдено</p>
            )}
        </div>
    )
}
