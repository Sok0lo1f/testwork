import clsx from 'clsx'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import classes from './Pagination.module.css'

interface PaginationProps {
    activePage: number
}
export const Pagination = ({ activePage }: PaginationProps) => {
    const { pages } = useTypedSelector((state) => state.postReducer)
    const nav = useNavigate()

    const next = () => {
        if (activePage === pages) return
        nav(`/${activePage + 1}`)
    }
    const prev = () => {
        if (activePage === 1) return
        nav(`/${activePage - 1}`)
    }
    return (
        <div className={classes.Pagination}>
            <button className={classes.Button} onClick={prev}>
                Назад
            </button>
            <ul className={classes.PagList}>
                {Array.from({ length: pages }).map((_, idx) => (
                    <li
                        key={idx}
                        className={clsx(
                            classes.PagList__Item,
                            activePage === idx + 1 && classes.Active
                        )}
                        onClick={() => nav(`/${idx + 1}`)}>
                        {idx + 1}
                    </li>
                ))}
            </ul>
            <button className={classes.Button} onClick={next}>
                Далее
            </button>
        </div>
    )
}
