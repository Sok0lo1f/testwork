import React from 'react'

export const Loader = () => {
    return (
        <div className='loader-wrapper'>
            <div className='lds-dual-ring'></div>
        </div>
    )
}
