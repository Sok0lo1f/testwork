import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import SearchIco from '../../assets/search.svg'
import { postActions } from '../../store/reducers/postsReducer'
import { AppDispatch } from '../../store/store'
import classes from './Input.module.css'

export const Input = () => {
    const [value, setValue] = useState('')
    const dispatch = useDispatch<AppDispatch>()

    const onSearch = () => {
        dispatch(postActions.searchPost(value.trim()))
        // setValue('')
    }
    return (
        <div className={classes.InputWrapper}>
            <input
                className={classes.Input}
                value={value}
                placeholder='Поиск'
                onChange={({ target }) => setValue(target.value)}
            />
            <button className={classes.Submit} onClick={onSearch}>
                <img src={SearchIco} alt='Search' />
            </button>
        </div>
    )
}
