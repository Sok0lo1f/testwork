import { createBrowserRouter, Navigate, RouterProvider } from 'react-router-dom'
import './App.css'
import { TablePage } from './pages/TablePage/TablePage'

const router = createBrowserRouter([
    {
        path: '/',
        index: true,
        element: <Navigate to='/1' />,
    },
    {
        path: ':page',
        element: <TablePage />,
    },
])

function App() {
    return <RouterProvider router={router} />
}

export default App
