export interface IPost {
    [key: string]: string | number
    userId: number
    id: number
    title: string
    body: string
}